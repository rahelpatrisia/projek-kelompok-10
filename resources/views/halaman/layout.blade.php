<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Regal</title>
  <!-- base:css -->
  <link rel="stylesheet" href="{asset("regal/vendors/mdi/css/materialdesignicons.min.css")}">
  <link rel="stylesheet" href="{{asset("regal/vendors/feather/feather.css")}}">
  <link rel="stylesheet" href={{asset("regal/vendors/base/vendor.bundle.base.css")}}>
  <!-- endinject -->
  <!-- plugin css for this page -->
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href={{asset("regal/css/style.css")}}>
  <!-- endinject -->
  <link rel="shortcut icon" href={{asset("regal/images/logor.png")}} />
</head>

<body>
   
  <div class="container-scroller">
    <!-- partial:../../partials/_navbar.html -->
    <nav class="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
      <div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center">
        <a class="navbar-brand brand-logo" href="/"><img src={{asset("regal/images/logo.svg")}} alt="logo"/></a>
        <a class="navbar-brand brand-logo-mini" href="/"><img src={{asset("regal/images/logo-mini.svg")}} alt="logo"/></a>
      </div>
      <div class="navbar-menu-wrapper d-flex align-items-center justify-content-end">
      
       
        <ul class="navbar-nav navbar-nav-right" >
            <li class="nav-item dropdown d-lg-flex d-none" href="/">
                <button type="button" class="btn btn-info font-weight-bold">+ Tambah Pertanyaan</button>
            </li>
          
        
    </nav>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      <!-- partial:../../partials/_sidebar.html -->
      <nav class="sidebar sidebar-offcanvas" id="sidebar">
        <div class="user-profile">
          <div class="user-image">
            <img src={{asset("regal/images/faces/face28.png")}}>
          </div>
          <div class="user-name">
              Edward Spencer
          </div>
          <div class="user-designation">
              Developer
          </div>
        </div>


        <ul class="nav">
          <li class="nav-item">
            <a class="nav-link" href="/oka">
              <i class="icon-head menu-icon"></i>
              <span class="menu-title">Profile</span>
            </a>
          </li>


        <ul class="nav">
          <li class="nav-item">
            <a class="nav-link" href={{asset("regal/index.html")}}>
              <i class="icon-box menu-icon"></i>
              <span class="menu-title">Beranda</span>
            </a>
          </li>

          <li class="nav-item">
            <a class="nav-link" href="/oki">
              <i class="icon-speech-bubble menu-icon"></i>
              <span class="menu-title">Pertanyaan</span>
            </a>
          </li>

          <li class="nav-item">
            <a class="nav-link" href="/mapels">
              <i class="icon-speech-bubble menu-icon"></i>
              <span class="menu-title">Mata Pelajaran</span>
            </a>
          </li>
     
          <li class="nav-item">
            <a class="nav-link" href="/Loogin">
              <span class="btn btn-danger">Login</span>
            </a>
          </li>

        </a>
    </li>
  </ul>
</nav>

<div class="main-panel">
     
        @yield('content')
  <div class="content-wrapper">
    <section class="contents">
    
    </section>
  </div>
    <!-- content-wrapper ends -->
    <!-- partial:partials/_footer.html -->
    <footer class="footer">
      <div class="d-sm-flex justify-content-center justify-content-sm-between">
        <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © bootstrapdash.com 2020</span>
        <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center"> Free <a href="https://www.bootstrapdash.com/" target="_blank">Bootstrap dashboard templates</a> from Bootstrapdash.com</span>
      </div>
      <span class="text-muted d-block text-center text-sm-left d-sm-inline-block mt-2">Distributed By: <a href="https://www.themewagon.com/" target="_blank">ThemeWagon</a></span> 
    </footer>
    
    <!-- partial -->
 
</div>

    
    
<!-- Default box -->
<!-- <div class="card">
  <div class="card-header">
    <h3 class="card-title">Title</h3>

    <div class="card-tools">
      <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
        <i class="fas fa-minus"></i>
      </button>
      <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
        <i class="fas fa-times"></i>
      </button>
    </div>
  </div> -->
 
  <!-- /.card-body -->
 
    <!-- Footer -->
  <!-- </div>
   /.card-footer
</div> -->
<!-- /.card -->

</section>