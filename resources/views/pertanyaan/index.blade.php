@extends('halaman.layout')

@section('title', 'List pertanyaan')

@section('content')

      <div class="row">
        <div class="col-sm-12 mb-4 mb-xl-0">
          <h4 class="font-weight-bold text-dark">Pertanyaan</h4>
          <a href="/pertanyaan/{{ $mapels->id }}/create" class="btn btn-primary mb-3">Tambah pertanyaan</a>
          <!-- <p class="font-weight-normal mb-2 text-muted">Pilih Topiknya ya!</p> -->
        
        </div>
        <table class="table">
            <thead class="thead-light">
              <tr>
                <th scope="col">#</th>
                <th scope="col">Pertanyaan</th>
                <th scope="col">Pelajaran</th>
                <th scope="col">Actions</th>
                
              </tr>
      </div>
      <div class="col-m-12 flex-column d-flex grid-margin stretch-card">
          <div class="row flex-grow">
            <div class="col-sm-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
      @forelse ($pertanyaans as $key=>$pertanyaans)
        
                  <tr>
                        <td>{{$key + 1}}</th>
                        <td>{{$pertanyaans->tanya}}</td>
                     
                        <td>{{$pertanyaans->mapels->nama}}</td>
                        <td>
                            <a href="/pertanyaan/{{ $mapels->id }}/{{$pertanyaans->id}}" class="btn btn-info">Show</a>
                            <a href="/pertanyaan/{{ $mapels->id }}/{{$pertanyaans->id}}/edit" class="btn btn-primary">Edit</a>
                            <form action="/pertanyaan/{{ $mapels->id }}/{{$pertanyaans->id}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <input type="submit" class="btn btn-danger my-1" value="Delete">
                            </form>
                        </td>
                    </tr>
                                    
                    </div>
                </div>
            </div>
            
          </div>
        </div>
        
        @empty
        <div class="col-m-12 flex-column d-flex grid-margin stretch-card">
          <div class="row flex-grow">
            <div class="col-sm-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                      <h4 class="text-dark font-weight-bold mb-2">No Data</h4>   
                  </div>
                </div>
            </div>
            
          </div>
        </div>
        @endforelse
</tbody>
</table>
@endsection