@extends('halaman.layout')

@section('title', 'Edit Mata Pelajaran')

@section('content')
<div>
        <h2>Edit Post {{$post->id}}</h2>
        <form action="/mapels/{{$mapels->id}}" method="POST">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label for="tanya">Edit pertanyaan</label>
                <input type="text" class="form-control" name="tanya" value="{{$post->tanya}}" id="tanya" placeholder="Edit pertanyaan">
                @error('tanya')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            
            <button type="submit" class="btn btn-primary">Edit</button>
        </form>
    </div>


@endsection
