@extends('halaman.layout')

@section('content')
<h2>Tambah Data</h2>
        <form action="/pertanyaan/{{ $mapels->id }}" method="POST">
            @csrf
            <div class="form-group">
                <label for="tanya">Pertanyaan</label>
                <input type="text" class="form-control" name="tanya" id="tanya" placeholder="Masukkan pertanyaan">
                @error('tanya')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
           
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>

@endsection