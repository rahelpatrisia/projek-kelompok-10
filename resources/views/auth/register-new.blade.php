<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Join Us!</title>
  <!-- base:css -->
  <link rel="stylesheet" href="{{asset('regal/vendors/mdi/css/materialdesignicons.min.css')}}">
  <link rel="stylesheet" href="{{asset('regal/vendors/feather/feather.css')}}">
  <link rel="stylesheet" href="{{asset('regal/vendors/base/vendor.bundle.base.css')}}">
  <!-- endinject -->
  <!-- plugin css for this page -->
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="{{asset('regal/css/style.css')}}">
  <!-- endinject -->
  <link rel="shortcut icon" href="{{asset('regal/images/logor.png')}}" />
</head>

<body>
  <div class="container-scroller">
    <div class="container-fluid page-body-wrapper full-page-wrapper">
      <div class="content-wrapper d-flex align-items-center auth px-0">
        <div class="row w-100 mx-0">
          <div class="col-lg-4 mx-auto">
            <div class="auth-form-light text-left py-5 px-4 px-sm-5">
              <div class="brand-logo">
                <img src="{{asset('regal/images/logo-dark.svg')}}" alt="logo">
              </div>
              <h2>Join Us!</h2>
              <h4>Let's Help Each Other</h4>
              <h6 class="font-weight-light"></h6>
              <form action="{{ route('register') }}" class="pt-3" method="post">
              @CSRF  
              <div class="form-group">
                  <input type="text" class="form-control form-control-lg" name="name" id="name" placeholder="Nama">
                  
                </div>
                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                <div class="form-group">
                  <input type="email" class="form-control form-control-lg" name="email" id="email" placeholder="Email">
                 
                </div>
                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                <div class="form-group">
                  <input type="password" class="form-control form-control-lg" name="password" id="password" placeholder="Password">
                  
                </div>
                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                <div class="form-group">
                  <input type="password" class="form-control form-control-lg" name="password_confirmation" id="password" placeholder="Konfirmasi">
                  
                </div>
                
                <div class="mt-3">
                  <a class="btn btn-block btn-info btn-lg font-weight-medium auth-form-btn" href="{{asset('regal/index.html')}}">SIGN UP</a>
                </div>
                <div class="text-center mt-4 font-weight-light">
                  Already have an account? <a href="{{ route('login') }}" class="text-primary">Login</a>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <!-- content-wrapper ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->
  <!-- base:js -->
  <script src="{{asset('regal/vendors/base/vendor.bundle.base.js')}}"></script>
  <!-- endinject -->
  <!-- inject:js -->
  <script src="{{asset('regal/js/off-canvas.js')}}"></script>
  <script src="{{asset('regal/js/hoverable-collapse.js')}}"></script>
  <script src="{{asset('regal/js/template.js')}}"></script>
  <!-- endinject -->
</body>

</html>
