@extends('halaman.layout')

@section('title', 'Edit Pertanyaan')

@section('content')
<div>
        <h2>Edit pertanyaanmu {{$post->id}}</h2>
        <form action="/mapels/{{$mapels->id}}" method="POST">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label for="nama">Edit</label>
                <input type="text" class="form-control" name="nama" value="{{$post->nama}}" id="nama" placeholder="Masukkan nama">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            
            <button type="submit" class="btn btn-primary">Edit</button>
        </form>
    </div>


@endsection
