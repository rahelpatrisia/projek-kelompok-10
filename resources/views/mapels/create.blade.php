@extends('halaman.layout')

@section('content')
<h2>Tambah Data</h2>
        <form action="/mapels" method="POST">
            @csrf
            <div class="form-group">
                <label for="nama">nama</label>
                <input type="text" class="form-control" name="nama" id="nama" placeholder="Masukkan nama">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
           
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>

@endsection