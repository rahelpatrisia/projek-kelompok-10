@extends('halaman.layout')

@section('title', 'List Posts')

@section('content')

      <div class="row">
        <div class="col-sm-12 mb-4 mb-xl-0">
          <h4 class="font-weight-bold text-dark">Hi, Yuk Mulai Bertanya!</h4>
          <p class="font-weight-normal mb-2 text-muted">Pilih Topiknya ya!</p>
        </div>
      </div>
      @forelse ($mapels as $key=>$mapels)
        <div class="col-xl-3 flex-column d-flex grid-margin stretch-card">
          <div class="row flex-grow">
            <div class="col-sm-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                      <h4 class="text-dark font-weight-bold mb-2">{{$mapels->nama}}</h4>  
                      <a href="/mapels/{{$mapels->id}}" class="btn btn-info">Show</a>
                      <a href="/pertanyaan/{{$mapels->id}}" class="btn btn-primary">Lihat</a> 
                  </div>
                </div>
            </div>
            
          </div>
        </div>
        @empty
        <div class="col-xl-3 flex-column d-flex grid-margin stretch-card">
          <div class="row flex-grow">
            <div class="col-sm-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                      <h4 class="text-dark font-weight-bold mb-2">No Data</h4>   
                  </div>
                </div>
            </div>
            
          </div>
        </div>
        @endforelse


@endsection