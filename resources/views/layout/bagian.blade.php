@extends('halaman.layout')
    
@section('content')
    

<div class="col-9 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <h1>Tambah Pertanyaan</h1> <br><br>
       
        <form class="forms-sample">
          <div class="form-group">
            <label for="exampleInputName1">Nama</label>
            <input type="text" class="form-control" id="exampleInputName1" placeholder="Name">
          </div>
        
          <div class="form-group">
            <label for="exampleSelectGender">Kelas</label>
            <select class="form-control form-control-lg" id="exampleFormControlSelect2">
                <option>Kelas</option>
                <option>SD (1-6)</option>
                <option>SMP (7-9)</option>
                <option>SMA (10-12)</option>
              </select>
            </div>
          <div class="form-group">
            <label for="exampleTextarea1">Pertanyaan</label>
            <textarea class="form-control" id="exampleTextarea1" rows="4" placeholder="Tulis pertanyaanmu"></textarea>
          </div>
          <button type="submit" class="btn btn-primary mr-2">Submit</button>
          <button class="btn btn-light">Cancel</button>
        </form>
      </div>
    </div>
  </div>
  @endsection