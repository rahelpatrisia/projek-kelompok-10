@extends('halaman.layout')
    
@section('content')
    
<div class="col-9 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <h4 class="card-title">Profile</h4>
        <p class="card-description">
          Edit Profil
        </p>
        <form class="forms-sample">
          <div class="form-group">
            <label for="exampleInputName1">Nama</label>
            <input type="text" class="form-control" id="exampleInputName1" placeholder="Nama">
          </div>
          <div class="form-group">
            <label for="exampleInputEmail3">Email</label>
            <input type="email" class="form-control" id="exampleInputEmail3" placeholder="Email">
          </div>
          <div class="form-group">
            <label for="exampleInputPassword4">Password</label>
            <input type="password" class="form-control" id="exampleInputPassword4" placeholder="Password">
          </div>
          <div class="form-group">
            <label for="exampleSelectGender">Kelas</label>
            <select class="form-control form-control-lg" id="exampleFormControlSelect2">
                <option>Kelas</option>
                <option>SD (1-6)</option>
                <option>SMP (7-9)</option>
                <option>SMA (10-12)</option>
              </select>
            </div>
         
          <button type="submit" class="btn btn-primary mr-2">Submit</button>
          <button class="btn btn-light">Cancel</button>
        </form>
      </div>
    </div>
  </div>
@endsection