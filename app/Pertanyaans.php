<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pertanyaans extends Model
{
    protected $fillable = ['tanya', 'foto', 'mapels_id'];


    // public function kritiks(){
    //     return $this->hasMany(Kritik::class);
    // }
    public function mapels(){
        return $this->belongsTo(Mapels::class);
    }

}
