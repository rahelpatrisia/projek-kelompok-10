<?php

namespace App\Http\Controllers;

use App\Mapels;
use Illuminate\Http\Request;

class MapelsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth')->except(['index','show']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $mapels = Mapels::all();
        return view('mapels.index', compact('mapels'));
    }

    /**
     * Show the form for creating a new resource.
     * untuk membuat data
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('mapels.create');

    }

    /**
     * Store a newly created resource in storage.
     * menyimpan data
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $request->validate([
            'nama'=> 'required',
        ]);

        Mapels::create([
            'nama' => $request->nama
        ]);

        return redirect('/mapels');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Mapels $mapels)
    {
        //
        
        return view('mapels.show', compact('mapels'));

    }

    /**
     * Show the form for editing the specified resource.
     * mengubah data
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(apels $mapels)
    {
        //
      
        return view('mapels.edit', compact('mapels'));
    }

    public function update(Mapels $mapels, Request $request)
    {
        $request->validate([
            'mapels' => 'required|unique:mapels'
        ]);

      
        $mapels->nama = $request->nama;
        $mapels->update();
        return redirect('/mapels');
        
    }

    /**
     * Remove the specified resource from storage.
     * menghapus data
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(mapels $mapels)
    {
       
        $mapels->delete();
        return redirect()->route('mapels.index');
    }
}
