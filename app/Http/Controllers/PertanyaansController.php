<?php

namespace App\Http\Controllers;

use App\Pertanyaans;
use App\Mapels;
use Illuminate\Http\Request;

class PertanyaansController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Mapels $mapels)
    {
        $pertanyaans = $mapels->pertanyaans;
        dd($pertanyaans[0]->mapels);
        return view('pertanyaan.index', compact('pertanyaans','mapels'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Mapels $mapels)
    {
        return view('pertanyaan.create', compact('mapels'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Mapels $mapels)
    {
        $request->validate([
            'tanya'=> 'required',
            'foto'=> 'required',
            
        ]);
     
        Film::create([
            'tanya' => $request->tanya,
            'foto' => $request->foto,
            'mapels_id' => $mapels->id
        ]);

        return redirect()->route('pertanyaan.index', compact("mapels"));
  
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Pertanyaans  $pertanyaans
     * @return \Illuminate\Http\Response
     */
    public function show(Mapels $mapels, Pertanyaans $pertanyaans)
    {
        return view('mapels.show', compact('mapels'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Pertanyaans  $pertanyaans
     * @return \Illuminate\Http\Response
     */
    public function edit(Pertanyaans $pertanyaans)
    {
        //
        return view('mapels.edit', compact('mapels'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Pertanyaans  $pertanyaans
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Mapels $mapels, Pertanyaans $pertanyaans)
    {
        //
        $request->validate([
            'nama' => 'required|unique:mapels'
        ]);

      
        $mapels->nama = $request->nama;
        $mapels->update();
        return redirect('/mapels');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Pertanyaans  $pertanyaans
     * @return \Illuminate\Http\Response
     */
    public function destroy(Mapels $mapels, Pertanyaans $pertanyaans)
    {
        $mapels->delete();
        return redirect()->route('mapels.index');
    }
}
