<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

 Route::get('/', 'LogController@indey');

 Route::get('/Loogin', 'FormController@inka');

Route::get('/oki', function(){
    return view('layout.bagian');
});

Route::get('/oka', function(){
    return view('layout.profile');
});

Route::get('/okay', function(){
    return view('authw.register');
});
// Route::get('/mapell', function(){
//     return view('layout.beranda');
// });
// Route::get('/okayy', function(){
//     return view('authw.login');
// });



Route::group([
    'prefix' => 'mapels',
    'as' => 'mapels.'
], function() {
    Route::get('/', 'MapelsController@index')->name('index');
    Route::get('/create', 'MapelsController@create')->name('create');
    Route::post('/', 'MapelsController@store')->name('store');
    Route::get('/{mapels}', 'MapelsController@show')->name('show');
    Route::get('/{mapels}/edit', 'MapelsController@edit')->name('edit');
    Route::put('/{mapels}', 'MapelsController@update')->name('update');
    Route::delete('/{mapels}', 'MapelsController@destroy')->name('destroy');
});


Route::group([
    'prefix' => 'pertanyaan',
    'as' => 'pertanyaan.'
], function() {
    Route::get('/{mapels}', 'PertanyaansController@index')->name('index');
    Route::get('/{mapels}/create', 'PertanyaansController@create')->name('create');
    Route::post('/{mapels}', 'PertanyaansController@store')->name('store');
    Route::get('/{mapels}/{pertanyaan}', 'PertanyaansController@show')->name('show');
    Route::get('/{mapels}/{pertanyaan}/edit', 'PertanyaansController@edit')->name('edit');
    Route::put('/{mapels}/{pertanyaan}', 'PertanyaansController@update')->name('update');
    Route::delete('/{mapels}/{pertanyaan}', 'PertanyaansController@destroy')->name('destroy');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
